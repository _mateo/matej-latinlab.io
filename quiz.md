---
layout: quiz
title: Web Typography Quiz
permalink: /web-typography-quiz/
exclude: true
image: /assets/img/quiz/web-typography-quiz.jpg
custom-js: 
  - https://surveyjs.azureedge.net/1.0.1/survey.jquery.min.js
  - /assets/js/quiz.min.js
body-class: quiz-page
---

Quiz
