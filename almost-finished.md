---
layout: page-bare
title: Almost finished
permalink: /almost-finished/
intro: Hey, I just need to confirm your email so please click on the link that was just sent to you. 
---

[Go to the homepage](/)
