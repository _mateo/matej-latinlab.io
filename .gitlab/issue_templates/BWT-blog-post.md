## Goals

* **Double organic traffic to the website by end-of-year '19 (3,000 visitors)**
  * Improve the SEO of the website (Jekyll SEO)
  * Make more content available (5-10 posts by end of August)
* **Increase traffic of the website to at least ~20k visitors per month for 3 months in a row by August**
  * Publish new posts regularly (1-2 per month)

---

### Post ideas

- Typography book reviews
- Newsletters
- Inspiration/teardowns (examples of websites with great typography and why it works)
- Typography for design systems articles
  - Vertical rhythm for web typography in web applications
- Posts about cool fonts ('Awesome geometric typefaces')
- Tools (and how to use them)
- Guides 

---

### Post title: 

**Short description**: 

#### Articles & resources

- item

---

### Submit

* [ ] [Sidebar.io](sidebar.io)
* [ ] Prototypr.io
* [ ] [Muzli app](https://muz.li/contact/)
* [ ] [Webdesignernews.com](webdesignernews.com)
* [ ] [Web designer depot](https://www.webdesignerdepot.com/contact/)
* [ ] [Hacker news](https://news.ycombinator.com/submit)
* [ ] [Designernews.co](designernews.co)
* [ ] [heydesigner.com](https://discord.gg/6sXuh4B)
* [ ] [Speckyboy](https://speckyboy.com/contact/)
* [ ] [uxdesignweekly.com](http://uxdesignweekly.com/contact/)
* [ ] [css weekly](https://css-weekly.com/submit-a-link/)
* [ ] [Frontend Front](https://frontendfront.com/)

/label ~Blog ~Traffic ~Aggregators ~SEO ~Campaign
/assign @matejlatin 