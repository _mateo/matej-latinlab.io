**Send date: [mmm dd, yyyy]**


Checklist:
* [ ] Non-customers campaign
* [ ] Customers campaign
* [ ] Blog post

### News

- Donations
- News item

### Featured

- Featured article

### Font of the month

- Font

### New resources

- Resource

### Articles

- Articles

### Did you know?

- An interesting fact about typography

Ideas

* Display vs text typefaces
* Grotesuq name origin
* Futura on the moon
* Arial is a copy of Helvetica
* Serif vs sans-serif readability myth

---

**Content ideas:**

- inspiration/gallery
- featured top donor
- cool new fonts
- added new resources
- quick tips
- feature book reviews
- sketch tips

**Sources:**

- https://www.typemag.org/
- [variable font articles at the bottom](https://blog.prototypr.io/an-exploration-of-variable-fonts-37f85a91a048)
- https://fonts.google.com/specimen/IBM+Plex+Sans

/label ~Newsletter ~Campaign ~Traffic