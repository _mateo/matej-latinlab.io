---
layout: page
title: All Posts
permalink: /all-posts/
exclude: true
intro: All web typography articles, newsletters and other posts published on this website.
---

{%- if site.posts.size > 0 -%}
  <div class="group">
    <div class="post-list">
      <hr>
      <ul class="">
        {%- for post in site.posts -%}
          {% unless post.exclude %}
            {%- include post-list.html type="general" intro="yes" -%}
          {% endunless %}
        {%- endfor -%}
      </ul>
    </div>
  </div>
{%- endif -%}

<p class="rss-subscribe">Subscribe <a href="{{ "/feed.xml" | relative_url }}">via RSS</a></p>
