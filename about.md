---
layout: page
title: About
permalink: /about/
active: about
intro: The story of Better Web Type—the complete web typography source for web designers and web developers.
comments: false
---

{%- include post/image-float-right.html url="/assets/img/matej-latin-portrait.jpg" caption="<strong>Matej Latin</strong> <br> Senior UX Designer at GitLab / <a href='https://twitter.com/matejlatin'>@matejlatin</a>" -%}

I never studied design, I’m a self-taught designer. That means that, among other skills, typography is something I had to learn by myself. When I finally started to explore this broad topic, I had no idea where to start. So I started where most people do: online and books. At the time, there weren’t many online resources that would also cover the topic of web typography. So I read books about using typography in print and lots of fragmented online content about best practices on the web. It took a while but after spending countless hours studying this intriguing art, I finally felt I understand how typography works.

Back then, I wished I had access to a complete and practical approach to learning about web typography. A source that would cover the whole process of web design and everyone involved in it—and that’s where the idea for Better Web Type came from.

### Better Web Typography for a Better Web

Two years later, in February 2017, I finally launched the “Better Web Typography for a Better Web” email course. 10,000 people signed up for it in the following month alone. Web designers, web developers, product managers, content managers, and even psychology professors signed up and praised the simplicity of the language used and the complete and practical approach to explaining things.

Equilateral Triangle of a Perfect Paragraph is a theory and a learning game I developed for the course as a simple and practical way to explain how to shape typographically correct paragraphs. People loved these explanations so much so that they started asking for more. They started suggesting that I should write a book about web typography. So I extended the content of around 13,000 words that I had for the course to more than 40,000 and published it in a fully-fledged book. A web typography book for both—web designers and web developers, and it now comes with a printable cheat sheet, example design files for designers and example source code for developers.

If you don’t know where to start, take the web typography quiz and find out how much you know about typography and how it applies to the web.

—Matej Latin 
