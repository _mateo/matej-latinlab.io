---
layout: resource
title:  "One File, Many Options"
date:   2019-02-28 16:46:56 +0000
exclude: true
author: Ollie Williams
link: https://css-tricks.com/one-file-many-options-using-variable-fonts-web/
description: Using Variable Fonts on the Web.
categories: 
  - Resources 
  - Variable-Fonts
tags:
  - Guide
---

{{ page.author }}
