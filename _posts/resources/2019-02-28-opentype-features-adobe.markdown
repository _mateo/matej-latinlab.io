---
layout: resource
title:  "Syntax for OpenType Features in CSS"
date:   2019-02-28 16:46:56 +0000
exclude: true
author: Adobe
link: https://helpx.adobe.com/typekit/using/open-type-syntax.html
description: Individual OpenType features and how to use them in CSS.
categories: 
  - Resources 
  - OpenType
tags:
  - Glossary
  - Frontend
---

{{ page.author }}
