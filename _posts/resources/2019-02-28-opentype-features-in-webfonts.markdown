---
layout: resource
title:  "OpenType Features in Web Fonts"
date:   2019-02-28 16:46:56 +0000
exclude: true
author: Alex Muñoz
link: http://iambravo.com/tipo/
description: A list of all OpenType features with examples.
categories: 
  - Resources 
  - OpenType
tags:
  - Cheatsheet
---

{{ page.author }}
