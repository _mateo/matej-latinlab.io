---
layout: resource
title:  "Font Playground"
date:   2019-02-28 16:46:56 +0000
exclude: true
author: Wenting Zhang
link: https://play.typedetail.com/
description: Play around with variable fonts.
categories: 
  - Resources 
  - Variable-Fonts
tags:
  - Tool
  - Frontend
---

{{ page.author }}
