---
layout: resource
title:  "Type Classifications"
date:   2019-02-28 16:46:56 +0000
exclude: true
author: Allan Haley
link: https://www.fonts.com/content/learning/fontology/level-1/type-anatomy/type-classifications
description: An overview of type classifications.
categories: 
  - Resources 
  - Recognizing-Choosing-Fonts
tags:
  - Guide
---

{{ page.author }}
