---
layout: resource
title: A Guide to Rhythm in Web Typography
date: 2019-02-28T16:46:56.000+00:00
exclude: true
author: Matej Latin
link: "/articles/2018/10/15/rhythm-in-web-typography/"
description: Vertical and horizontal rhythm in web typography.
image: "/assets/img/resources/bwt-rhythm@2x.jpg"
categories:
- Resources
- Better-Web-Type
tags:
- Guide

---
{{ page.author }}