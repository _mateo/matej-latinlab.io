---
layout: resource
title:  "TypeKit Tweaker"
date:   2019-02-28 16:46:56 +0000
exclude: true
author: kylefox.ca
link: https://chrome.google.com/webstore/detail/typekit-tweaker/imjkefgbldfcncjipjoahpbeakfjflgf
description: A toolbar that lets you adjust Typekit font previews.
categories: 
  - Resources 
  - Recognizing-Choosing-Fonts
tags:
  - Plugin
  - Chrome
---

{{ page.author }}
