---
layout: resource
title:  "Measure"
date:   2019-02-28 16:46:56 +0000
exclude: true
author: Rowan Cavanagh Stein
link: https://chrome.google.com/webstore/detail/measure/bbompmbliibpeaaloikpoahdokhjdmeg
description: Measure typographic line lengths right from the page.
categories: 
  - Resources 
  - Basics
tags:
  - Plugin
  - Chrome
---

{{ page.author }}
