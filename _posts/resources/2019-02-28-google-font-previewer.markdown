---
layout: resource
title:  "Google Font Previewer for Chrome"
date:   2019-02-28 16:46:56 +0000
exclude: true
author: Pamela Fox
link: https://chrome.google.com/webstore/detail/google-font-previewer-for/engndlnldodigdjamndkplafgmkkencc
description: Lets you choose a font from the Google Font directory with a few text styling options, and preview them on the current tab.
categories: 
  - Resources 
  - Recognizing-Choosing-Fonts
tags:
  - Plugin
  - Chrome
---

{{ page.author }}
