---
layout: resource
title:  "Modular Scale (Sass)"
date:   2019-02-28 16:46:56 +0000
exclude: true
author: Scott Kellum
link: https://github.com/modularscale/modularscale-sass
description: Modular scale calculator built into your Sass.
categories: 
  - Resources 
  - Visual-Hierarchy
tags:
  - Tool
  - Frontend
---

{{ page.author }}
