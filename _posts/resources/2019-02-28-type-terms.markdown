---
layout: resource
title:  "Type Terms"
date:   2019-02-28 16:46:56 +0000
exclude: true
author: Supremo
link: https://www.supremo.co.uk/typeterms/
description: The animated typographic cheat sheet.
categories: 
  - Resources 
  - Typeface-Anatomy
tags:
  - Glossary
---

{{ page.author }}
