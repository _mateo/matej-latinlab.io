---
layout: resource
title:  "Variable Fonts"
date:   2019-02-28 16:46:56 +0000
exclude: true
author: Nick Sherman
link: https://v-fonts.com/
description: A simple resource for finding and trying variable fonts.
categories: 
  - Resources 
  - Variable-Fonts
tags:
  - Gallery
---

{{ page.author }}
