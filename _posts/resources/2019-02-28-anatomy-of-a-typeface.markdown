---
layout: resource
title:  "Anatomy of a Typeface"
date:   2019-02-28 16:46:56 +0000
exclude: true
author: Typedia
link: http://typedia.com/learn/only/anatomy-of-a-typeface/
description: Learn about the anatomy of typefaces.
categories: 
  - Resources 
  - Typeface-Anatomy
tags:
  - Glossary
---

{{ page.author }}
