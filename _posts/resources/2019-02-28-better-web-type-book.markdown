---
layout: resource
title:  "Better Web Typography for a Better Web"
date:   2019-02-28 16:46:56 +0000
exclude: true
author: Matej Latin
link: /web-typography-book/
description: A comprehensive web typography book for web designers and web developers.
image: /assets/img/resources/bwt-book@2x.jpg
categories: 
  - Resources 
  - Better-Web-Type
tags:
  - Book
---

{{ page.author }}
