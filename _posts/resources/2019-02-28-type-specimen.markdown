---
layout: resource
title:  "Web Font Specimen"
date:   2019-02-28 16:46:56 +0000
exclude: true
author: Tim Brown
link: http://webfontspecimen.com/
description: Web Font Specimen is a handy, free resource web designers and type designers can use to see how typefaces will look on the web.
categories: 
  - Resources 
  - Recognizing-Choosing-Fonts
tags:
  - Tool
  - Frontend
---

{{ page.author }}
