---
layout: post-newsletter
title: Cool new stuff in web typography in August ’19
title-short: August ’19
categories:
- Newsletter
tags:
- Free-Font
- Accessibility
- Design-Systems
date: 2019-08-13T14:06:57.000+00:00
author: matej
toc: true
intro: |2-

  Omega Sans is the featured free font of this edition, we take a look at recognising font styles, dynamic type in Airbnb apps and typography in design systems.
edition: '012'
image: "/assets/img/newsletter/012/post@2x.jpg"

---
How is it going? Did you get a chance to go on holiday yet? I finally had some time to rest and took the last week off, that’s why this newsletter is one week late.

<h2 class="h3">News</h2> 

1. I added back a [sample of the Better Web Type book](https://betterwebtype.com/better-web-typography-for-a-better-web-sample.pdf) to the website. Feel free to check it out, I hope it will convince you to [buy the full book](https://betterwebtype.com/web-typography-book/?ref=bwt-newsletter-aug-19).
2. I wrote another blog post about quantifying UX for the GitLab blog. This time about the placement of the Clone button on GitLab. [Check it out here](https://about.gitlab.com/2019/07/26/quantifying-ux-positioning-of-the-clone-button/?ref=betterwebtype).

<h2 class="h3">Featured</h2>

#### A guide to recognising font styles

Telling font styles apart was one of the hardest things to do when I started working as a designer. Being self-taught, the only major difference I could see was that a font was either a serif or a sans-serif. But the more I explored, the more I realised how vast the universe of font styles actually is.

[Check it out →](https://betterwebtype.com/articles/2019/07/14/recognising-font-styles/?utm_source=newsletter&utm_medium=email&utm_campaign=bwt-newsletter-aug-19)

<h2 class="h3">Font of the month</h2>

{% include post/image.html url="/assets/img/newsletter/012/font@2x.jpg" %}

#### **Omega Sans**

Designed by Anthony James, this is a classic sans-serif typeface built with balanced Bezier curves. You can get the [regular weight for free](https://freebiesbug.com/free-fonts/omega-sans/?ref=betterwebtype) but the whole family comes in [4 weights and only costs $10](https://www.ajtypography.com/shop/omega-sans-free-font?ref=betterwebtype). This is a great bargain for a cool font that can add some uniqueness to a website.

<h2 class="h3">Cool Articles</h2>

#### [Supporting dynamic type at AirBnb](https://medium.com/airbnb-engineering/supporting-dynamic-type-at-airbnb-b47c68b0c998?ref=betterwebtype)

I loved this case study about making the type in AirBnb apps dynamic and thus making them more accessible. [Read more →](https://medium.com/airbnb-engineering/supporting-dynamic-type-at-airbnb-b47c68b0c998?ref=betterwebtype)

#### [Building a design system—where to start?](https://uxdesign.cc/building-a-design-system-where-to-start-part-4-typography-5065b8d360c?ref=betterwebtype)

A series of posts about building a design system. I love that it start with typography 👍[Read more →](https://uxdesign.cc/building-a-design-system-where-to-start-part-4-typography-5065b8d360c?ref=betterwebtype)

#### [Type in the digital era is a mess](https://www.fastcompany.com/90340882/type-in-the-digital-era-is-a-mess?ref=betterwebtype)

A cool post from Marcin Wichary although a bit long. I’m really glad he raises certain points about how hard it is to design with digital type and the reasons behind these difficulties. [Read more →](https://www.fastcompany.com/90340882/type-in-the-digital-era-is-a-mess?ref=betterwebtype)

<h2 class="h3">Did you know?</h2>  
<div class="group no-bottom-margin"> <div class="col-6 right-pad-20px no-bottom-margin"> <p>Have you ever noticed the difference in weights of horizontal versus vertical lines in letters? Optical corrections like these make the letters look more balanced which has a huge impact on the overall appearance of a text. Letter T without optical corrections (left) and with optical corrections (right).</p> </div> <div class="col-3 no-bottom-margin"> <img src="/assets/img/newsletter/012/optical-corrections.jpg" /> </div></div>

<h2 class="h3">Photo of the month</h2>  
Got to see some cool examples of typography from early 20th century in the Slovenian gallery of modern art 🙌 [@matejlatin](https://www.instagram.com/matejlatin/).

<blockquote class="instagram-media" data-instgrm-permalink="https://www.instagram.com/p/B0v8YG-BcU4/" data-instgrm-version="12" style=" background:#FFF; border:0; border-radius:3px; box-shadow:0 0 1px 0 rgba(0,0,0,0.5),0 1px 10px 0 rgba(0,0,0,0.15); margin: 1px; max-width:540px; min-width:326px; padding:0; width:99.375%; width:-webkit-calc(100% - 2px); width:calc(100% - 2px);"><div style="padding:16px;"> <a href="https://www.instagram.com/p/B0v8YG-BcU4/" style=" background:#FFFFFF; line-height:0; padding:0 0; text-align:center; text-decoration:none; width:100%;" target="_blank"> <div style=" display: flex; flex-direction: row; align-items: center;"> <div style="background-color: #F4F4F4; border-radius: 50%; flex-grow: 0; height: 40px; margin-right: 14px; width: 40px;"></div> <div style="display: flex; flex-direction: column; flex-grow: 1; justify-content: center;"> <div style=" background-color: #F4F4F4; border-radius: 4px; flex-grow: 0; height: 14px; margin-bottom: 6px; width: 100px;"></div> <div style=" background-color: #F4F4F4; border-radius: 4px; flex-grow: 0; height: 14px; width: 60px;"></div></div></div><div style="padding: 19% 0;"></div> <div style="display:block; height:50px; margin:0 auto 12px; width:50px;"><svg width="50px" height="50px" viewBox="0 0 60 60" version="1.1" xmlns="https://www.w3.org/2000/svg" xmlns:xlink="https://www.w3.org/1999/xlink"><g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd"><g transform="translate(-511.000000, -20.000000)" fill="#000000"><g><path d="M556.869,30.41 C554.814,30.41 553.148,32.076 553.148,34.131 C553.148,36.186 554.814,37.852 556.869,37.852 C558.924,37.852 560.59,36.186 560.59,34.131 C560.59,32.076 558.924,30.41 556.869,30.41 M541,60.657 C535.114,60.657 530.342,55.887 530.342,50 C530.342,44.114 535.114,39.342 541,39.342 C546.887,39.342 551.658,44.114 551.658,50 C551.658,55.887 546.887,60.657 541,60.657 M541,33.886 C532.1,33.886 524.886,41.1 524.886,50 C524.886,58.899 532.1,66.113 541,66.113 C549.9,66.113 557.115,58.899 557.115,50 C557.115,41.1 549.9,33.886 541,33.886 M565.378,62.101 C565.244,65.022 564.756,66.606 564.346,67.663 C563.803,69.06 563.154,70.057 562.106,71.106 C561.058,72.155 560.06,72.803 558.662,73.347 C557.607,73.757 556.021,74.244 553.102,74.378 C549.944,74.521 548.997,74.552 541,74.552 C533.003,74.552 532.056,74.521 528.898,74.378 C525.979,74.244 524.393,73.757 523.338,73.347 C521.94,72.803 520.942,72.155 519.894,71.106 C518.846,70.057 518.197,69.06 517.654,67.663 C517.244,66.606 516.755,65.022 516.623,62.101 C516.479,58.943 516.448,57.996 516.448,50 C516.448,42.003 516.479,41.056 516.623,37.899 C516.755,34.978 517.244,33.391 517.654,32.338 C518.197,30.938 518.846,29.942 519.894,28.894 C520.942,27.846 521.94,27.196 523.338,26.654 C524.393,26.244 525.979,25.756 528.898,25.623 C532.057,25.479 533.004,25.448 541,25.448 C548.997,25.448 549.943,25.479 553.102,25.623 C556.021,25.756 557.607,26.244 558.662,26.654 C560.06,27.196 561.058,27.846 562.106,28.894 C563.154,29.942 563.803,30.938 564.346,32.338 C564.756,33.391 565.244,34.978 565.378,37.899 C565.522,41.056 565.552,42.003 565.552,50 C565.552,57.996 565.522,58.943 565.378,62.101 M570.82,37.631 C570.674,34.438 570.167,32.258 569.425,30.349 C568.659,28.377 567.633,26.702 565.965,25.035 C564.297,23.368 562.623,22.342 560.652,21.575 C558.743,20.834 556.562,20.326 553.369,20.18 C550.169,20.033 549.148,20 541,20 C532.853,20 531.831,20.033 528.631,20.18 C525.438,20.326 523.257,20.834 521.349,21.575 C519.376,22.342 517.703,23.368 516.035,25.035 C514.368,26.702 513.342,28.377 512.574,30.349 C511.834,32.258 511.326,34.438 511.181,37.631 C511.035,40.831 511,41.851 511,50 C511,58.147 511.035,59.17 511.181,62.369 C511.326,65.562 511.834,67.743 512.574,69.651 C513.342,71.625 514.368,73.296 516.035,74.965 C517.703,76.634 519.376,77.658 521.349,78.425 C523.257,79.167 525.438,79.673 528.631,79.82 C531.831,79.965 532.853,80.001 541,80.001 C549.148,80.001 550.169,79.965 553.369,79.82 C556.562,79.673 558.743,79.167 560.652,78.425 C562.623,77.658 564.297,76.634 565.965,74.965 C567.633,73.296 568.659,71.625 569.425,69.651 C570.167,67.743 570.674,65.562 570.82,62.369 C570.966,59.17 571,58.147 571,50 C571,41.851 570.966,40.831 570.82,37.631"></path></g></g></g></svg></div><div style="padding-top: 8px;"> <div style=" color:#3897f0; font-family:Arial,sans-serif; font-size:14px; font-style:normal; font-weight:550; line-height:18px;"> View this post on Instagram</div></div><div style="padding: 12.5% 0;"></div> <div style="display: flex; flex-direction: row; margin-bottom: 14px; align-items: center;"><div> <div style="background-color: #F4F4F4; border-radius: 50%; height: 12.5px; width: 12.5px; transform: translateX(0px) translateY(7px);"></div> <div style="background-color: #F4F4F4; height: 12.5px; transform: rotate(-45deg) translateX(3px) translateY(1px); width: 12.5px; flex-grow: 0; margin-right: 14px; margin-left: 2px;"></div> <div style="background-color: #F4F4F4; border-radius: 50%; height: 12.5px; width: 12.5px; transform: translateX(9px) translateY(-18px);"></div></div><div style="margin-left: 8px;"> <div style=" background-color: #F4F4F4; border-radius: 50%; flex-grow: 0; height: 20px; width: 20px;"></div> <div style=" width: 0; height: 0; border-top: 2px solid transparent; border-left: 6px solid #f4f4f4; border-bottom: 2px solid transparent; transform: translateX(16px) translateY(-4px) rotate(30deg)"></div></div><div style="margin-left: auto;"> <div style=" width: 0px; border-top: 8px solid #F4F4F4; border-right: 8px solid transparent; transform: translateY(16px);"></div> <div style=" background-color: #F4F4F4; flex-grow: 0; height: 12px; width: 16px; transform: translateY(-4px);"></div> <div style=" width: 0; height: 0; border-top: 8px solid #F4F4F4; border-left: 8px solid transparent; transform: translateY(-4px) translateX(8px);"></div></div></div> <div style="display: flex; flex-direction: column; flex-grow: 1; justify-content: center; margin-bottom: 24px;"> <div style=" background-color: #F4F4F4; border-radius: 4px; flex-grow: 0; height: 14px; margin-bottom: 6px; width: 224px;"></div> <div style=" background-color: #F4F4F4; border-radius: 4px; flex-grow: 0; height: 14px; width: 144px;"></div></div></a><p style=" color:#c9c8cd; font-family:Arial,sans-serif; font-size:14px; line-height:17px; margin-bottom:0; margin-top:8px; overflow:hidden; padding:8px 0 7px; text-align:center; text-overflow:ellipsis; white-space:nowrap;"><a href="https://www.instagram.com/p/B0v8YG-BcU4/" style=" color:#c9c8cd; font-family:Arial,sans-serif; font-size:14px; font-style:normal; font-weight:normal; line-height:17px; text-decoration:none;" target="_blank">A post shared by matejlatin (@matejlatin)</a> on <time style=" font-family:Arial,sans-serif; font-size:14px; line-height:17px;" datetime="2019-08-04T16:40:37+00:00">Aug 4, 2019 at 9:40am PDT</time></p></div></blockquote> <script async src="//www.instagram.com/embed.js"></script>

__

That’s it for this month, see you in September! 👋

Cheers,<br>Matej