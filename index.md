---
# Feel free to add content and custom Front Matter to this file.
# To modify the layout, see https://jekyllrb.com/docs/themes/#overriding-theme-defaults

layout: course
title: Free Course
active: course
body-class: "course hasForm"
image: "/assets/img/course/lesson1.jpg"
comments: false
---
