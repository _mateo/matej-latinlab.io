---
layout: page
title: Refund policy
permalink: "/refunds/"
active: ''
intro: ''

---
## Returns

Our policy lasts 30 days. If 30 days have gone by since your purchase, unfortunately we can’t offer you a refund or exchange.

To complete your return, we require a receipt or proof of purchase.

## Refunds

Once your return is received and inspected, we will send you an email to notify you that we have received your returned item. We will also notify you of the approval or rejection of your refund.

If you are approved, then your refund will be processed, and a credit will automatically be applied to your credit card or original method of payment, within a certain amount of days.

## Late or missing refunds

If you haven’t received a refund yet, first check your bank account again.  
Then contact your credit card company, it may take some time before your refund is officially posted.

Next contact your bank. There is often some processing time before a refund is posted.  
If you’ve done all of this and you still have not received your refund yet, please contact us at hello@matejlatin.co.uk.